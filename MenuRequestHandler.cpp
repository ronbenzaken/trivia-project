#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(LoggedUser loggedUser) : m_user(loggedUser)
{

}
bool MenuRequestHandler::isRequestRelevant(Request req)
{
	int Id = req.id;
	if ( (Id == getPlayerResult)||(Id == logoutR) || (Id == getRoomsR) || (Id == getPlayersInRoomR )|| (Id == getHighScoreR )|| (Id == joinRoomR )|| (Id == createRoomR))
	{
		return true;
	}
	return false;
}

RequestResult MenuRequestHandler::handleRequest(Request req)
{
	RequestResult res;
	int id = req.id;
	switch (id){

	case logoutR:
		res = signout(req);
		break;

	case getRoomsR:
		res = getRooms(req);
		break;
		
	case getPlayersInRoomR:
		res = getPlayersInRoom(req);
		break;
		
	case joinRoomR:
		res = joinRoom(req);
		break;
	
	case createRoomR:
		res = createRoom(req);
		break;
	
	case getHighScoreR:
		res = getHighscores(req);
		break;
	case getPlayerResult:
		res = this->getPlayerScore(req);
		break;
	}
	return res;
}
RequestResult MenuRequestHandler::signout(Request req)
{
	RequestResult ans;
	LogoutResponse res;
	res.status = goodResponse;
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	ans.newHandler = (IRequestHandler*)m_handlerFacroty.createLoginRequestHandler();
	return ans;
}
RequestResult MenuRequestHandler::getPlayerScore(Request req)
{
	GetPlayerScoresResponse res;
	RequestResult ans;
	ans.newHandler = m_handlerFacroty.createMenuRequestHandler(m_user);;
	res.status = goodResponse;
	res.stats = m_highscoreTable.getUsesrStat(m_user.getName());
	ans.newHandler = m_handlerFacroty.createMenuRequestHandler(m_user);
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	return ans;
}
RequestResult MenuRequestHandler::getRooms(Request req)
{
	RequestResult ans;
	GetRoomResponse res;
	res.rooms = m_roomManager.getRooms();
	if (res.rooms.size() == 0)
	{
		res.status = zeroRooms;
	}
	else
		res.status = goodResponse;
		
	ans.newHandler = m_handlerFacroty.createMenuRequestHandler(m_user);
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	return ans;
}
RequestResult MenuRequestHandler::getPlayersInRoom(Request req)
{
	RequestResult ans;
	std::vector<LoggedUser> users;
 	Room room;
	GetPlayersInRoomResponse res;
	GetPlayersInRoomRequest request= JsonRequestPacketDeserializer::deserializerGetPlayersInRoomRequest(req.buffer);
	room = m_roomManager.getRoom(request.roomId);
	users = room.getAllUsers();
	int i = 0;
	for (i = 0; i < users.size(); i++)
	{
		res.rooms.push_back(users[i].getName());
	}
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	ans.newHandler = m_handlerFacroty.createMenuRequestHandler(m_user);
	return ans;
}
RequestResult MenuRequestHandler::getHighscores(Request req)
{

	RequestResult ans;
	HighScoreResponse res;
	res.highScores = m_highscoreTable.getHighScores();
	if (res.highScores.size() == 0)
	{
		res.status = emptyTable;
	}
	else
	{
		res.status = goodResponse;

	}
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	ans.newHandler = m_handlerFacroty.createMenuRequestHandler(m_user);
	return ans;
}
RequestResult MenuRequestHandler::joinRoom(Request req)
{

	RequestResult ans;
	JoinRoomRequest request = JsonRequestPacketDeserializer::deserializerJoinRoomRequest(req.buffer);
	JoinRoomResponse res;
	int tryToEnter = m_roomManager.m_rooms[request.roomId].addUser(m_user);
	if (tryToEnter == 1)
	{
		res.status = goodResponse;
		ans.newHandler = (IRequestHandler*)m_handlerFacroty.createRoomMemberRequestHandler(m_user, m_roomManager.m_rooms[request.roomId]);//change next time;

	}
	else if (tryToEnter == 0)
	{
		res.status = fullRoom;
		ans.newHandler = m_handlerFacroty.createMenuRequestHandler(m_user);

	}
	else
	{
		res.status = roomInGame;
		ans.newHandler = m_handlerFacroty.createMenuRequestHandler(m_user);
	}	
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	return ans;
}
RequestResult MenuRequestHandler::createRoom(Request req)
{
	//check about num of question

	RequestResult ans;
	RoomData data;
	int roomId = 0;
	CreateRoomRequest request = JsonRequestPacketDeserializer::deserializerCreateRoomRequest(req.buffer);
	CreateRoomResponse res;
	data.timePerQuestion = request.answerTimeout;
	data.isActive = 0;
	data.maxPlayers = std::to_string(request.maxUsers);
	request.questionCount;
	data.name = request.roomName;
	data.questionCount = request.questionCount;
	m_roomManager.createRoom(m_user, data);
	ans.newHandler = ans.newHandler = (IRequestHandler*)m_handlerFacroty.createRoomAdminRequestHandler(m_user, m_roomManager.m_rooms[m_roomManager.id - 1]);//change next time;
	res.status = goodResponse;
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	return ans;
}