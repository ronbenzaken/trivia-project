#pragma once
#include "Response.h"
typedef struct GetQuestionResponse : public Response
{
	std::string question;
	std::vector<std::string> answers;

}getQuestionResponse;