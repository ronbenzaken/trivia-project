#include "RoomAdminRequsetHandler.h"
RoomAdminRequestHandler::RoomAdminRequestHandler(Room room, LoggedUser loggedUser) : m_user(loggedUser) , m_room(room)
{

}

bool RoomAdminRequestHandler::isRequestRelevant(Request req)
{
	if ((req.id == getRoomStateR) || (req.id == closeRoomR) || (req.id == startGameR))
		return true;
	return false;
}
RequestResult RoomAdminRequestHandler::handleRequest(Request req)
{
	RequestResult result;
	if (req.id == getRoomStateR)
	{
		result = this->getRoomState(req);
	}
	else if (req.id == closeRoomR)
	{
		result = this->closeRoom(req);
	}
	else
	{
		result = this->startGame(req);
	}
	return result;
}
RequestResult RoomAdminRequestHandler::closeRoom(Request req)
{

	RequestResult result;
	CloseRoomResponse res;
	res.status = goodResponse;
	string data = std::to_string(closedRoom);
	std::vector<LoggedUser> usersInRoom = m_roomManager.m_rooms[m_room.getRoomData().id].getAllUsers();
	int i = 0;
	for (i = 1; i < usersInRoom.size(); i++)
	{
		if (sockets.clients.find((usersInRoom[i])) != sockets.clients.end())
		{
			try
			{
				sockets.sendData(data, sockets.clients[(usersInRoom[i])]);
			}
			catch (...)
			{
				m_room.removeUser(usersInRoom[i]);
			}
		}
	}
	m_roomManager.deleteRoom(m_room.getRoomData().id);
	result.newHandler = (IRequestHandler*)m_handlerFacroty.createMenuRequestHandler(m_user);
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}
RequestResult RoomAdminRequestHandler::startGame(Request req)
{

	RequestResult result;
	StartGameResponse res;
	res.status = goodResponse;
	string data = std::to_string(startedGame);
	std::vector<LoggedUser> usersInRoom = m_roomManager.m_rooms[m_room.getRoomData().id].getAllUsers();
	int i = 0;
	for (i = 1; i < usersInRoom.size(); i++)
	{
		try
		{
			sockets.sendData(data, sockets.clients[(usersInRoom[i])]);
		}
		catch (...)
		{
			m_room.removeUser(usersInRoom[i]);
		}
	}
	result.newHandler = (IRequestHandler*)m_handlerFacroty.createGameRequestHandler(m_user, m_room);
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	m_roomManager.deleteRoom(m_room.getRoomData().id);
	return result; 
}
RequestResult RoomAdminRequestHandler::getRoomState(Request req)
{
	RequestResult result;
	GetRoomStateResponse res;
	std::vector<LoggedUser> users;
	RoomData roomD = m_roomManager.m_rooms[m_room.getRoomData().id].getRoomData();
	res.answertimeOut = roomD.timePerQuestion;
	res.hasGameBegun = roomD.isActive;
	users = m_room.getAllUsers();
	for (int i = 0; i < users.size(); i++)
	{
		res.players.push_back(users[i].getName());
	}
	res.questionCount = roomD.questionCount;
	res.status = goodResponse;
	result.newHandler = m_handlerFacroty.createRoomAdminRequestHandler(m_user, m_room);
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}