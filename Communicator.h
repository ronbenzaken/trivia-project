#pragma once
#pragma comment(lib, "Ws2_32.lib")

#include <iostream>
#include <thread>
#include "Response.h"
#include <deque>
#include <queue>
#include <map>
#include <mutex>
#include <string>
#include <condition_variable>
#include <WinSock2.h>
#include <Windows.h>
#include "RequestHandlerFactory.h"
#include "IRequestHandler.h"

#define PORT 7777

class Communicator
{
public:
	Communicator();
	~Communicator();
	void bindAndListen();
	void handleRequests();
private:
	ClientsMapSocket sockets;
	//void sendData(SOCKET sc, Buffer message);
	//char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);
	//int getIntPartFromSocket(SOCKET sc, int bytesNum);
	std::map <SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory m_handlerfactory;
	void startThreadForNewClient(SOCKET);
	SOCKET _serverSocket;
};

