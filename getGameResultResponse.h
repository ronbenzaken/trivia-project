#pragma once
#include "Response.h"
#include "PlayerResult.h"
typedef struct getGameResultResponse : public Response
{
	std::vector<PlayerResult> results;
}getGameResultResponse;