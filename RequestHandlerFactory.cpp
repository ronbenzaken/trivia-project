#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequsetHandler.h"
#include "RoomMemberRequsetHandler.h"
#include "GameRequestHandler.h"

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	LoginRequestHandler* answer = new LoginRequestHandler();
	return answer;
}
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser loggedU)
{
	MenuRequestHandler* answer = new MenuRequestHandler(loggedU);
	return answer;
}
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser loggedUser, Room room)
{
	RoomAdminRequestHandler* ans = new RoomAdminRequestHandler(room, loggedUser);
	return ans;
}
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser loggedUser, Room room)
{
	RoomMemberRequestHandler* ans = new RoomMemberRequestHandler(room, loggedUser);
	return ans;
}
GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser user, Room room)
{
	GameRequestHandler* ans = new GameRequestHandler(room , user);
	return ans;

}