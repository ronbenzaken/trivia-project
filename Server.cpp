#include "Server.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"
Server::Server(IDataAccess& dataAccess) : m_dataBase(dataAccess)
{
	m_dataBase.open();
	this->m_communicator = Communicator();
	this->m_handlerFactory = RequestHandlerFactory();
}

void Server::run()
{
	this->m_communicator.bindAndListen();
	// create new thread for handling message
	std::thread tr(&Server::m_handlerFactory, this);
	tr.detach();
	this->m_communicator.handleRequests();

}