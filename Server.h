#pragma once
#include "Communicator.h"
#include "IDataAccess.h"
class Server
{
private:
	IDataAccess& m_dataBase;
	Communicator m_communicator;
	RequestHandlerFactory m_handlerFactory;
public:
	Server(IDataAccess& m_dataBase);
	void run();
};