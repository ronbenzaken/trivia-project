#pragma once
#include <vector>
#include <map>
#include "GameData.h"
#include "Question.h"
#include "LoggedUser.h"
#include "PlayerResult.h"
class Game
{

private:
	std::vector<Question> m_question;
	std::map<LoggedUser, GameData> m_players;
	int gameId;
public:
	Game();
	int getPlayerNum();
	Game(std::vector<Question>,int id);
	std::vector<PlayerResult> getGameResult();
	Question getQuestionForUser(LoggedUser user);
	void removePlayer(LoggedUser user);
	void addPlayer(LoggedUser user);
	bool submitAnswer(LoggedUser user, int ansId);
	bool operator==(Game other);
	int getGameId();
};
