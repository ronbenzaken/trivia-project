#include "Communicator.h"

void sendData(SOCKET sc, Buffer message)
{
	const char* data = message.c_str();
	if (send(sc, message.c_str(), message.size(), 0) == INVALID_SOCKET)
	{
		throw ("Error while sending message to client");
	}
}
char* getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return "";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket";
		throw (s);
	}

	data[bytesNum] = 0;
	return data;
}
int getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	s;
	return atoi(s);
}
Communicator::Communicator()
{
	WSADATA wsaData = { 0 };
	int iResult = 0;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}
Communicator::~Communicator()
{

}
void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

										// again stepping out to the global namespace
										// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw (__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;

}
void Communicator::handleRequests()
{
	SOCKET tempClient;
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		tempClient = ::accept(_serverSocket, NULL, NULL);
		if (tempClient == INVALID_SOCKET)
		{
			throw (__FUNCTION__);
		}

		m_clients[tempClient] = (IRequestHandler*)m_handlerfactory.createLoginRequestHandler();
		std::thread tr(&Communicator::startThreadForNewClient, this, tempClient);
		tr.detach();
		std::cout << "Waiting for client connection request" << std::endl;
	}
}

void Communicator::startThreadForNewClient(SOCKET socket)
{
	int size = 0,i = 0, requestType = 0;
	Buffer requestData;
	RequestResult requestAnswer;
	Request tempReq;
	char* msg = "";
	char* num = "";
	string status, name, digitLen;
	while (true)
	{
		msg = "";
		status.clear();
		requestData.clear();
		requestAnswer.response.clear();
		digitLen.clear();
		try
		{
			requestType = getIntPartFromSocket(socket, 2);
			
			if ((requestType == loginR) || (requestType == signupR) || (requestType == createRoomR))
			{
				num = getPartFromSocket(socket, 1, 0);
				while (num[0] != '#')
				{
					digitLen.push_back(num[0]);
					num = getPartFromSocket(socket, 1, 0);
				}
				msg = getPartFromSocket(socket, std::stoi(digitLen), 0);
			}
			else if (requestType == submitAnswerR)
			{
				msg = getPartFromSocket(socket, 1, 0);
			}
			else if ((getPlayersInRoomR == requestType) || (joinRoomR == requestType))
			{
				num = getPartFromSocket(socket, 1, 0);
				while (num[0] != '#')
				{
					digitLen.push_back(num[0]);
					num = getPartFromSocket(socket, 1, 0);
				}
				msg = (char*)digitLen.c_str();
			}
		}
		catch (string e)
		{
			//close socket ignore
			m_clients.erase(m_clients.find(socket));
			break;
		}
		requestData += std::string(msg);
		tempReq.id = requestType;
		tempReq.buffer = requestData;
		
		if (m_clients[socket]->isRequestRelevant(tempReq))
		{
			requestAnswer = m_clients[socket]->handleRequest(tempReq);
			if ((tempReq.id == getGameResR) || (tempReq.id == getQuestionR) || (tempReq.id == submitAnswerR))
			{
				
			}
			else
			{
				delete(m_clients[socket]);
				m_clients[socket] = requestAnswer.newHandler;
			}
			if (tempReq.id == changeToGameHandler || tempReq.id == changeToMenuHandler)
			{

			}
			else
			{
				if (tempReq.id == loginR || tempReq.id == signupR)
				{
					name.clear();
					status.push_back(requestAnswer.response[0]);
					status.push_back(requestAnswer.response[1]);
					if (std::stoi(status) == goodResponse)
					{
						for (int i = 0; requestData[i] != '#'; i++)
						{
							name.push_back(requestData[i]);
						}
						LoggedUser user(name);
						sockets.clients[user] = socket;
					}
				}
				else if (logoutR == requestType)
				{
					sockets.clients.erase(LoggedUser(name));
				}
				try
				{
					sendData(socket, requestAnswer.response);
				}
				catch (...)
				{
					//remove from map and logged user
					sockets.clients.erase(sockets.clients.find(LoggedUser(name)));
					try 
					{
						m_clients.erase(m_clients.find(socket));
					}
					catch (...)
					{
						break;
					}
					break;//cant send data socket disconnected;
				}
				
			}
		}
		else
		{
			try
			{
				sockets.clients.erase(sockets.clients.find(LoggedUser(name)));
				sendData(socket, "18Illigeal request!");
			}			
			catch (...)
			{

			}
			m_clients.erase(m_clients.find(socket));
			break;
		}
	}
	delete(m_clients[socket]);
}