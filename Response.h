#pragma once

enum Status
{
	goodResponse = 20,
	signupError = 11,
	loginError = 12,
	zeroRooms = 13, 
	emptyTable = 14, 
	fullRoom = 15,
	roomInGame = 16,
	closedRoom = 17,
	startedGame = 18,
	userInSystem =19

};

struct Response
{

	Status status;

} typedef Response;
