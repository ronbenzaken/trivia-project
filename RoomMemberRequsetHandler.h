#pragma once
#include "IRequestHandler.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"
#include "Room.h"
class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(Room room, LoggedUser loggedUser);
	virtual bool isRequestRelevant(Request req);
	virtual RequestResult handleRequest(Request req);
private:
	Room m_room;
	LoggedUser m_user;
	RoomManager m_roomManager;
	RequestHandlerFactory m_handlerFacroty;
	RequestResult leaveRoom(Request req);
	RequestResult getRoomState(Request req);
	RequestResult roomHasBeenClosed();
	RequestResult  gameHasBeenStarted();

};

