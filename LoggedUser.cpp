#include "LoggedUser.h"

LoggedUser::LoggedUser(const std::string name)
{
	this->name = name;
}
std::string LoggedUser::getName() const
{
	return this->name;
}
bool LoggedUser::operator<(const LoggedUser& a) const
{
	if (this->name < a.getName())
	{
		return true;
	}
	return false;
}
bool LoggedUser::operator==(const LoggedUser& a) const
{
	if (this->name == a.getName())
	{
		return true;
	}
	return false;
}