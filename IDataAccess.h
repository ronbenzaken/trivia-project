#pragma once
#include "Question.h"
#include "NewUser.h"
#include "LoggedUser.h"
#include "UserStats.h"
#include <list>
#include <map>
class IDataAccess 
{
private:

public:
	virtual bool open() = 0;
	virtual ~IDataAccess() = default;
	virtual bool doesUserExist(const string) = 0;
	virtual std::list<Question> getQuestions(const int ) = 0;
	virtual void insertNewUser(const NewUser newUser) = 0;
	virtual void increaseGame(const string name) = 0;
	virtual void increaseRAns(const string name, int) = 0;
	virtual void increaseWGame(const string name) = 0;
	virtual void increaseWAns(const string name, int) = 0;
	virtual bool login(string name, string pass) = 0;
	virtual std::map<LoggedUser, int> getHighScores() = 0;
	virtual UserStats getUserStats(string name) = 0;

};

