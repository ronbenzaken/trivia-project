#pragma once
#include "Response.h"
#include <vector>
typedef struct GetRoomStateResponse : public Response
{
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answertimeOut;
} GetRoomStateResponse;