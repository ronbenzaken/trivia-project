#pragma once
#include "IRequestHandler.h"
#include <iostream>
#include "RequestHandlerFactory.h"
#include "LoginRequest.h"
#include "SignupRequest.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
//class RequestHandlerFactory;
class LoginRequestHandler : public IRequestHandler
{
public:
	virtual bool isRequestRelevant(Request req) override;
	virtual RequestResult handleRequest(Request req) override;
private:
	LoginManager m_loginManager;
	RequestHandlerFactory m_handlerFacroty;
	RequestResult login(Request req);
	RequestResult signup(Request req);
};
