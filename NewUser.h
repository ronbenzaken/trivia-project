#pragma once
#include <iostream>

using std::string;

class NewUser
{
private:
	string pass;
	string name;
	string email;
public:
	NewUser(string, string, string);
	string getEmail() const;
	string getPass() const;
	string getName() const;

};