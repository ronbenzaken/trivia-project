#pragma once

#include <iostream>
#include <vector>
#include <bitset>
#include <ctime>

typedef std::string Buffer;

enum RequestId
{
	loginR = 10,
	signupR = 11,
	logoutR = 12,
	getRoomsR = 13,
	getPlayerResult = 28,
	getPlayersInRoomR = 14,
	getHighScoreR = 15,
	joinRoomR = 16,
	createRoomR = 17,
	leaveRoomR = 18,
	getRoomStateR = 19,
	closeRoomR = 20,
	startGameR = 21,
	changeToMenuHandler = 22,
	changeToGameHandler = 23,
	submitAnswerR = 24,
	getQuestionR = 25,
	getGameResR = 26,
	leaveGameR = 27
};

struct Request
{
public:
	int id;
	//ctime receivalTime;
	Buffer buffer;

} typedef Request;
