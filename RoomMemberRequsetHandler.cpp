#include "RoomMemberRequsetHandler.h"
RoomMemberRequestHandler::RoomMemberRequestHandler(Room room, LoggedUser loggedUser) : m_user(loggedUser), m_room(room)
{

}
bool RoomMemberRequestHandler::isRequestRelevant(Request req)
{
	if (req.id == leaveRoomR || req.id == getRoomStateR)
	{
		return true;
	}
	else if (req.id == changeToMenuHandler)
	{
		return true;
	}
	else if (req.id == changeToGameHandler)
	{
		return true;
	}
	return false;
}
RequestResult RoomMemberRequestHandler::roomHasBeenClosed()
{
	RequestResult result;
	result.newHandler = (IRequestHandler*)m_handlerFacroty.createMenuRequestHandler(m_user);
	return result;
}
RequestResult  RoomMemberRequestHandler::gameHasBeenStarted()
{
	RequestResult result;
	result.newHandler = (IRequestHandler*)m_handlerFacroty.createGameRequestHandler(m_user, m_room);
	return result;
}
RequestResult RoomMemberRequestHandler::handleRequest(Request req)
{

	RequestResult result;

	if (req.id == leaveRoomR)
	{
		result = leaveRoom(req);
	}
	else if (req.id == changeToMenuHandler)
	{
		result = roomHasBeenClosed();
	}
	else if (req.id == changeToGameHandler)
	{
		result = gameHasBeenStarted();
	}
	else
	{
		result = getRoomState(req);
	}
	return result;
}

RequestResult RoomMemberRequestHandler::leaveRoom(Request req)
{
	RequestResult result;
	LeaveRoomResponse res;
	m_roomManager.removeUser(m_room.getRoomData().id, m_user);
	res.status = goodResponse;
	result.newHandler = (IRequestHandler*)m_handlerFacroty.createMenuRequestHandler(m_user);
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}

RequestResult RoomMemberRequestHandler::getRoomState(Request req)
{
	RequestResult result;
	GetRoomStateResponse res;
	std::vector<LoggedUser> users;
	RoomData roomD = m_roomManager.m_rooms[m_room.getRoomData().id].getRoomData();
	res.answertimeOut = roomD.timePerQuestion;
	res.hasGameBegun = roomD.isActive;
	users = m_room.getAllUsers();
	for (int i = 0; i < users.size(); i++)
	{
		res.players.push_back(users[i].getName());
	}
	res.questionCount = roomD.questionCount;
	res.status = goodResponse;
	result.newHandler = m_handlerFacroty.createRoomMemberRequestHandler(m_user ,m_room);
	result.response = JsonResponsePacketSerializer::serializeResponse(res);
	return result;
}

