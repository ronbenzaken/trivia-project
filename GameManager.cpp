#include "GameManager.h"
#include "DataBaseAccess.h"

int GameManager::gameId = 0;
std::vector<Game> GameManager::m_games;

GameManager::GameManager()
{
	this->m_dataBase = new DataBaseAccess;
}

int GameManager::createGame(Room room)
{
	std::list<Question> l = this->m_dataBase->getQuestions(room.getRoomData().questionCount);
	std::vector <Question> v{ std::begin(l), std::end(l) };
	std::vector<LoggedUser> users;
	int i = 0;
	auto a = l.begin();
	Question temp = (*a);
	Game game(v, gameId);
	gameId++;
	users = room.getAllUsers();
	for (i = 0; i < users.size(); i++)
	{
		game.addPlayer(users[i]);
		m_dataBase->increaseGame(users[i].getName());
	}
	m_games.push_back(game);
	return game.getGameId();
}

void GameManager::increaseWAns(LoggedUser user, int amount)
{
	m_dataBase->increaseWAns(user.getName(), amount);
}
void GameManager::increaseRAns(LoggedUser user, int amount)
{
	m_dataBase->increaseRAns(user.getName(), amount);
}
void GameManager::deleteGame(Game game)
{
	for (auto it = m_games.begin();m_games.size() > 0 && it != m_games.end(); it++)
	{
		if (game == (*it))
		{
			it = m_games.erase(it);
			break;
		}
	}
}

void GameManager::removeUser(int id, LoggedUser user)
{

	auto it = m_games.begin();
	for (it; it != m_games.end(); it++)
	{
		if (id == (*it).getGameId())
		{
			(*it).removePlayer(user);
			if ((*it).getPlayerNum() == 0)
			{
				this->deleteGame(*it);
			}
			break;
		}
	}

}
std::vector<PlayerResult> GameManager::getGameResult(int id)
{

	auto it = m_games.begin();
	for (it; it != m_games.end(); it++)
	{
		if (id == (*it).getGameId())
		{
			return (*it).getGameResult();
		}
	}

}
void GameManager::submitAnswer(int id, LoggedUser user, int ansId)
{

	auto it = m_games.begin();
	for (it; it != m_games.end(); it++)
	{
		if (id == (*it).getGameId())
		{
			if ((*it).submitAnswer(user, ansId))
			{
				m_dataBase->increaseRAns(user.getName(), 1);
			}
			else
				m_dataBase->increaseWAns(user.getName(), 1);
		}
	}
}
Question GameManager::getQuestionForUser(int id, LoggedUser user)
{
	auto it = m_games.begin();
	Question a;
	for (it; it != m_games.end(); it++)
	{
		if (id == (*it).getGameId())
		{
			a.setAns((*it).getQuestionForUser(user).getAns());
			a.setQues((*it).getQuestionForUser(user).getQues());
			a.setWAns1((*it).getQuestionForUser(user).getWAns1());
			a.setWAns2((*it).getQuestionForUser(user).getWAns2());
			a.setWAns3((*it).getQuestionForUser(user).getWAns3());

		}
	}
	return a;
}