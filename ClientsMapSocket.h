#pragma once
#include <map>
#include <algorithm>
#include <iostream>
#include "LoggedUser.h"
#include <WinSock2.h>

using std::map;
using std::string;

class ClientsMapSocket
{
public:
	void sendData(string message, SOCKET sock);
	static map<LoggedUser, SOCKET> clients;
};
