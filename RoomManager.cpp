#include "RoomManager.h"
using std::vector;
int RoomManager::id = 0;
std::map<int, Room>  RoomManager::m_rooms;


void RoomManager::createRoom(LoggedUser admin, RoomData data)
{
	data.id = this->id;
	Room temp(data);
	int k = temp.addUser(admin);
	std::pair<int, Room> a (id , temp);
	this->m_rooms.insert(a);
	this->id++;
}
Room RoomManager::getRoom(int id)
{
	return m_rooms[id];
}
void RoomManager::deleteRoom(int id)
{
	this->m_rooms.erase(id);
}

int RoomManager::getRoomState(int id)
{
	return m_rooms[id].getRoomData().isActive;
}
void RoomManager::startGame(int id)
{
	m_rooms[id].changeRoomState();
}
void RoomManager::gameEnd(int id)
{
	m_rooms[id].changeRoomState();
}
std::vector<RoomData> RoomManager::getRooms()
{
	vector<RoomData> rooms;
	std::map<int, Room>::iterator it;
	for (it = this->m_rooms.begin(); it != this->m_rooms.end(); it++)
	{
		if((*it).second.getRoomData().id < 100 && (*it).second.getRoomData().id >= 0)
			rooms.push_back(it->second.getRoomData());
	}
	return rooms;
}
void RoomManager::removeUser(int id, LoggedUser user)
{
	this->m_rooms[id].removeUser(user);
}