#pragma once

#include <iostream>
#include <map>
#include "Room.h"
#include <algorithm>

class RoomManager
{
public:
	void createRoom(LoggedUser admin, RoomData data);
	void deleteRoom(int id);
	int getRoomState(int id);
	void removeUser(int id, LoggedUser user);
	void startGame(int id);
	void gameEnd(int id);
	Room getRoom(int id);
	std::vector<RoomData> getRooms();  // need to check if RoomMetadata is RoomData
	static std::map<int, Room> m_rooms; // need to replace int to roomID.
	static int id;

private:

};
