#include "JsonResponsePacketSerializer.h"
Buffer JsonResponsePacketSerializer::serializeResponse(LoginResponse res)
{
	std::string buffer;
	buffer = std::to_string(res.status);
	return buffer;

	//json j = res;
	//return (Buffer)json::to_bson(j);
}
Buffer JsonResponsePacketSerializer::serializeResponse(SignupResponse res)
{
	std::string buffer;
	buffer = std::to_string(res.status);
	return buffer;
	//json j = res;
	//return (Buffer)json::to_bson(j);
}
Buffer JsonResponsePacketSerializer::serializeResponse(ErrorResponse res)
{
	//json j = res;
	//return (Buffer)json::to_bson(j);
	std::string buffer;
	buffer = res.err;
	return buffer;

}
Buffer JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse res)
{
	std::string buffer, ans;
	int i = 0, size = res.rooms.size();
	for (i = 0; i < res.rooms.size(); i++)
	{
		buffer.push_back('#');
		buffer += res.rooms[i];
	}

	if (buffer.size() > 1)
		ans += std::to_string(buffer.size());
	ans += buffer;
	return ans;
}
Buffer JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse res)
{
	std::string buffer;
	buffer = std::to_string(res.status);
	return buffer;
}
Buffer JsonResponsePacketSerializer::serializeResponse(LogoutResponse res)
{
	std::string buffer;
	buffer = std::to_string(res.status);
	return buffer;
}
Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse res)
{
	std::string buffer, ans;
	int i = 0;
	ans = std::to_string(res.status);
	if(res.status == goodResponse)
	{
		for (i = 0; i < res.rooms.size(); i++)
		{
			buffer.push_back('#');
			buffer += std::to_string(res.rooms[i].id);
			buffer.push_back(',');
			buffer += std::to_string(res.rooms[i].isActive);
			buffer.push_back(',');
			buffer += res.rooms[i].maxPlayers;
			buffer.push_back(',');
			buffer += res.rooms[i].name;
			buffer.push_back(',');
			buffer += std::to_string(res.rooms[i].timePerQuestion);
			buffer.push_back(',');
			buffer += std::to_string(res.rooms[i].questionCount);

		}
		if (buffer.size() > 1)
			ans += std::to_string(buffer.size());
		ans += buffer;
	}
	return ans;
}
Buffer JsonResponsePacketSerializer::serializeResponse(HighScoreResponse res)
{
	std::string buffer, ans;
	ans = std::to_string(res.status);
	//std::pair<LoggedUser, int> tempPair();
	if (res.status == goodResponse)
	{

		int i = 0;
		for (i = 0; i < res.highScores.size(); i++)
		{
			buffer.push_back('#');
			buffer += res.highScores[i].first.getName();
			buffer.push_back(',');
			buffer += std::to_string(res.highScores[i].second);
		}
		if (buffer.size() > 1)
			ans += std::to_string(buffer.size());
		ans += buffer;

	}
	return ans;
}
Buffer JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse res)
{
	std::string buffer;
	buffer = std::to_string(res.status);
	return buffer;
}

Buffer JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse res)
{

	std::string buffer;
	buffer = std::to_string(res.status);
	return buffer;
 }
 Buffer JsonResponsePacketSerializer::serializeResponse(StartGameResponse res)
 {

	 std::string buffer;
	 buffer = std::to_string(res.status);
	 return buffer;
 }
 Buffer JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse res)
 {

	 std::string buffer;
	 buffer = std::to_string(res.status);
	 return buffer;
 }

 Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse res)
{
	 std::string buffer = "", ans;
	 int i = 0;
	 ans = std::to_string(res.status);

	 if (res.status == goodResponse)
	 {
		 buffer.push_back('#');

		 if (res.hasGameBegun)
			 buffer += "t";
		 else
			 buffer += "f";
		 buffer.push_back('#');
		 buffer += res.players[0];
		 for (i = 1; i < res.players.size(); i++)
		 {
			 buffer.push_back(',');
			 buffer += res.players[i];
		 }
		 buffer.push_back('#');
		 buffer += std::to_string(res.questionCount);
		 if (buffer.size() > 1)
			 ans += std::to_string(buffer.size());
		 ans += buffer;
	 }
	 return ans;
}
Buffer JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse res)
{
	std::string buffer = "", ans;
	int i = 0;
	ans = std::to_string(res.status);
	if (res.status == goodResponse)
	{
		ans += std::to_string(res.correctAnswerId);
	}
	return ans;
}
Buffer JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse res)
{

	std::string buffer = "", ans;
	int i = 0;
	ans = std::to_string(res.status);
	if (res.status == goodResponse)
	{
		buffer += '#';
		buffer += res.question;
		buffer += '#';
		for (i = 0; i < 4; i++)
		{
			buffer += res.answers[i];
			buffer += '#';
		}
		if (buffer.size() > 1)
			ans += std::to_string(buffer.size());
		ans += buffer;
	}
	return ans;
}
Buffer JsonResponsePacketSerializer::serializeResponse(getGameResultResponse res)
{

	std::string buffer = "", ans;
	int i = 0, size = res.results.size();
	ans = std::to_string(res.status);
	if (res.status == goodResponse)
	{
		for (i = 0; i < size; i++)
		{
			buffer += '#';
			buffer += res.results[i].userName;
			buffer += ",";
			buffer += std::to_string(res.results[i].rightAnswerCount);
			//buffer += ",";
			//buffer += std::to_string(res.results[i].wrongAnswerCount);
		}
		if (buffer.size() > 1)
			ans += std::to_string(buffer.size());
		ans += buffer;
	}
	return ans;
}
Buffer JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse res)
{

	std::string buffer;
	buffer = std::to_string(res.status);
	return buffer;
}
Buffer JsonResponsePacketSerializer::serializeResponse(GetPlayerScoresResponse res)
{
	std::string buffer = "", ans;
	int i = 0;
	ans = std::to_string(res.status);
	if (res.status == goodResponse)
	{
		buffer += '#';
		buffer += std::to_string(res.stats.getGames());
		buffer += '#';
		buffer += std::to_string(res.stats.getRAns());
		buffer += '#';
		buffer +=std::to_string(res.stats.getWAns());
		if (buffer.size() > 1)
			ans += std::to_string(buffer.size());
		ans += buffer;
	}
	return ans;

}
