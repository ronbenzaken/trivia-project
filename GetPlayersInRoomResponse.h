#pragma once
#include <stdlib.h>
#include <vector>
#include "Response.h"
typedef struct GetPlayersInRoomResponse
{
	std::vector<std::string> rooms;
} GetPlayersInRoomResponse;