#include "Room.h"
std::vector<LoggedUser> Room::getAllUsers()
{
	return this->m_users;
}
int Room::addUser(LoggedUser user)
{
	if (std::stoi(this->m_metadata.maxPlayers) > m_users.size() && this->m_metadata.isActive == 0)
	{
		m_users.push_back(user);
		return 1;
	}
	return 0;
}
int Room::removeUser(LoggedUser user)
{
	std::vector<LoggedUser>::iterator it;
	it = std::find(m_users.begin(), m_users.end(), user);
	if (it != m_users.end())
	{
		m_users.erase(it);
		return 1;
	}
	return 0;
}
RoomData Room::getRoomData()
{
	return this->m_metadata;
}
Room::Room(const Room &room)
{
	m_metadata = room.m_metadata;
	m_users = room.m_users;
}

Room::Room() {}

void Room::changeRoomState()
{
	if (this->m_metadata.id == 0)
	{
		this->m_metadata.id = 1;
	}
	else
	{
		this->m_metadata.id = 0;
	}
}
Room::Room(RoomData data)
{
	this->m_metadata = data;
}