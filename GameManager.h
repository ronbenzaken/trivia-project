#pragma once
#include <iostream>
#include "Game.h"
#include "IDataAccess.h"
#include "Room.h"
class GameManager
{

private:
	static int gameId;
	IDataAccess* m_dataBase;
	static std::vector<Game> m_games; 
public:
	GameManager();
	int createGame(Room room);
	void increaseWAns(LoggedUser, int);
	void increaseRAns(LoggedUser, int);
	void deleteGame(Game game);
	void removeUser(int id, LoggedUser user);
	std::vector<PlayerResult> getGameResult(int id);
	void submitAnswer(int id, LoggedUser user, int ansId);
	Question getQuestionForUser(int id, LoggedUser user);

};