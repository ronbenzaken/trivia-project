#pragma once
#include <iostream>
#include <string>

using std::string;

class  Question
{
private:
	string ques;
	string ans;
	string WAns1;
	string WAns2;
	string WAns3;

public:
	Question operator=(Question ot);
	string getQues() const;
	string getAns() const;
	string getWAns1() const;
	string getWAns2() const;
	string getWAns3() const;
	void setQues(string a) ;
	void setAns(string a) ;
	void setWAns1(string a) ;
	void setWAns2(string a) ;
	void setWAns3(string a) ;
	Question(string ques, string ans, string WAns1, string WAns2, string WAns3);
	Question() {};
};

