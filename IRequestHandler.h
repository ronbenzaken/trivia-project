#pragma once

#include "Request.h"
#include "RequestResult.h"
#include "ClientsMapSocket.h"

struct RequestResult;
class IRequestHandler
{
public:
	virtual bool isRequestRelevant(Request req) = 0;
	virtual RequestResult handleRequest(Request req) = 0;
	ClientsMapSocket sockets;
};
