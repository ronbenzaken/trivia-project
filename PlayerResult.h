#pragma once
typedef struct PlayerResult
{
	
	std::string userName;
	unsigned int wrongAnswerCount;
	unsigned int rightAnswerCount;

}PlayerResult;