#include "LoginRequestHandler.h"
//#include "RequestHandlerFactory.h"

bool LoginRequestHandler::isRequestRelevant(Request req) 
{
	if (req.id == loginR || req.id == signupR)
	{
			return true;
	}
	return false;
}
RequestResult LoginRequestHandler::handleRequest(Request req) 
{
	RequestResult ans;
	if (req.id == loginR)
	{
		ans = login(req);
	}
	else if (req.id == signupR)
	{	
		ans = signup(req);
	}
	return ans;
}

RequestResult LoginRequestHandler::login(Request req)
{
	LoginResponse response;
	RequestResult answer;

	LoginRequest loginReq = JsonRequestPacketDeserializer::deserializerLoginRequest(req.buffer);
	answer.newHandler = NULL;
	if (this->sockets.clients.find(loginReq.userName) != sockets.clients.end())
	{
		response.status = userInSystem;
		answer.response = std::to_string(response.status);
		answer.newHandler = m_handlerFacroty.createLoginRequestHandler();
	}
	else

	{
		try
		{
			m_loginManager.logIn(loginReq.userName, loginReq.password);
		}
		catch (std::string e)
		{
			response.status = loginError;
			answer.response = std::to_string(response.status);
			answer.newHandler = m_handlerFacroty.createLoginRequestHandler();
		}
	}
	if (answer.newHandler == NULL)
	{
		response.status = goodResponse;
		answer.response = JsonResponsePacketSerializer::serializeResponse(response);
		answer.newHandler = (IRequestHandler*)(m_handlerFacroty.createMenuRequestHandler(LoggedUser(loginReq.userName)));
		
	}
	return answer;
}
RequestResult LoginRequestHandler::signup(Request req)
{
	SignupResponse response;
	RequestResult answer;
	
	answer.newHandler = NULL;
	SignupRequest signupReq = JsonRequestPacketDeserializer::deserializerSignupRequest(req.buffer);
	try
	{
		m_loginManager.signUp(signupReq.userName, signupReq.password, signupReq.email);
	}
	catch (std::string e)
	{
		response.status = signupError;
		answer.response = std::to_string(response.status);
		answer.newHandler = m_handlerFacroty.createLoginRequestHandler();
	}

	if(answer.newHandler == NULL)
	{
		response.status = goodResponse;
		answer.response = JsonResponsePacketSerializer::serializeResponse(response);
		answer.newHandler = (IRequestHandler*)(m_handlerFacroty.createMenuRequestHandler(LoggedUser(signupReq.userName)));//next version change level
	}
	return answer;
}
