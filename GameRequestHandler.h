#pragma once
#include "IRequestHandler.h"
#include "GameManager.h"
#include "RequestResult.h"
#include "RequestHandlerFactory.h"
#include "Room.h"
#include "Response.h"
#include "LoggedUser.h"
#include "JsonRequestPacketDeserializer.h"
#include "GetQuestionResponse.h"
#include "SubmitAnswerResponse.h"
#include "getGameResultResponse.h"
#include "JsonResponsePacketSerializer.h"

class GameRequestHandler : public IRequestHandler 
{
public:
	GameRequestHandler(Room room, LoggedUser user);
	virtual bool isRequestRelevant(Request req) override;
	virtual RequestResult handleRequest(Request req) override;
private:
	LoggedUser m_user;
	int gameId;
	Room m_room;
	GameManager m_gameManager;
	RequestHandlerFactory m_handlerFactory;

	RequestResult getQuestion(Request req);
	RequestResult submitAnswer(Request req);
	RequestResult getGameResult(Request req);
	RequestResult leaveGame(Request req);
};

