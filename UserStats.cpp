#include "UserStats.h"

UserStats::UserStats(const int game, const int gamesW, const int rAns, const int wAns)
{
	games = game;
	gamesWon = gamesW;
	RAns = rAns;
	WAns = wAns;
}
int UserStats::getGamesWon()const
{
	return gamesWon;
}
int UserStats::getGames()const
{
	return games;
}
int UserStats::getRAns()const
{
	return RAns;
}
int UserStats::getWAns() const
{
	return WAns;
}
UserStats::UserStats(const UserStats &other)
{
	this->games = other.games;
	this->gamesWon = other.gamesWon;
	this->RAns = other.RAns;
	this->WAns = other.WAns;
}
UserStats::UserStats()
{
	this->games = -1;
	this->gamesWon = -1;
	this->RAns = -1;
	this->WAns = -1;
}