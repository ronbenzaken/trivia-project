#pragma once
#include <iostream>
#include "Question.h"
typedef struct GameData
{
	Question currentQues;
	unsigned int wrongAnswerCount;
	unsigned int rightAnswerCount;
	GameData(Question q) : currentQues(q.getQues(), q.getAns(), q.getWAns1(), q.getWAns2(), q.getWAns3()) 
	{

	}
	GameData() {}
}GameData;