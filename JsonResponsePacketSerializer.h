#pragma once
#include "LoginResponse.h"
#include "SignupResponse.h"
#include "JoinRoomResponse.h"
#include "HighScoreResponse.h"
#include "GetRoomsResponse.h"
#include "LogoutResponse.h"
#include "GetPlayersInRoomResponse.h"
#include "CreateRoomResponse.h"
#include "ErrorResponse.h"
#include <iostream>
#include "Request.h"
#include "LeaveRoomResponse.h"
#include "LeaveGameResponse.h"
#include "StartGameResponse.h"
#include "CloseRoomResponse.h"
#include "GetRoomStateResponse.h"
#include "SubmitAnswerResponse.h"
#include "GetQuestionResponse.h"
#include "GetPlayerScoresResponse.h"
#include "getGameResultResponse.h"
#include <string>

class JsonResponsePacketSerializer
{
public:
	static Buffer serializeResponse(LoginResponse);
	static Buffer serializeResponse(SignupResponse);
	static Buffer serializeResponse(ErrorResponse);
	static Buffer serializeResponse(GetPlayersInRoomResponse);
	static Buffer serializeResponse(CreateRoomResponse);
	static Buffer serializeResponse(LogoutResponse);
	static Buffer serializeResponse(GetRoomsResponse);
	static Buffer serializeResponse(HighScoreResponse);
	static Buffer serializeResponse(JoinRoomResponse);
	static Buffer serializeResponse(LeaveRoomResponse);
	static Buffer serializeResponse(StartGameResponse);
	static Buffer serializeResponse(CloseRoomResponse);
	static Buffer serializeResponse(GetRoomStateResponse);
	static Buffer serializeResponse(SubmitAnswerResponse);
	static Buffer serializeResponse(GetQuestionResponse);
	static Buffer serializeResponse(getGameResultResponse);
	static Buffer serializeResponse(LeaveGameResponse);
	static Buffer serializeResponse(GetPlayerScoresResponse);


};