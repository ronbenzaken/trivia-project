#pragma once

#include "Response.h"
#include <vector>
#include "LoggedUser.h"


typedef struct HighScoreResponse : public Response
{
	std::vector<std::pair<LoggedUser, int>> highScores;
} HighScoreResponse;