#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "HighScoreTable.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"
#include "GetPlayerScoresResponse.h"
#include"JsonRequestPacketDeserializer.h"

class MenuRequestHandler : public IRequestHandler
{
public:
	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request);
	MenuRequestHandler(LoggedUser loggedUser);

private:
	LoggedUser m_user;
	RoomManager m_roomManager;
	HighScoreTable m_highscoreTable;
	RequestHandlerFactory m_handlerFacroty;

	RequestResult signout(Request);
	RequestResult getRooms(Request);
	RequestResult getPlayersInRoom(Request);
	RequestResult getHighscores(Request);
	RequestResult getPlayerScore(Request req);
	RequestResult joinRoom(Request);
	RequestResult createRoom(Request);
};
