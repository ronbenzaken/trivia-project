#pragma once
#include <iostream>
#include <vector>
#include "LoggedUser.h"
#include "DataBaseAccess.h"

class LoginManager
{
public:

	void signUp(std::string userName, std::string password, std::string email);
	void logIn(std::string userName, std::string password);
	void logOut();

private:

	DataBaseAccess m_database;
	std::vector<LoggedUser> m_loggedUsers;

};
