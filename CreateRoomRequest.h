#pragma once
#include <iostream>
typedef struct CreateRoomRequest {
	
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;

}CreateRoomRequest;