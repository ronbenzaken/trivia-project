#include "HighScoreTable.h"
#include "DataBaseAccess.h"
HighScoreTable::HighScoreTable()
{
	this->m_database = new DataBaseAccess;
}
std::vector<std::pair<LoggedUser, int>> HighScoreTable::getHighScores()
{
	std::vector<std::pair<LoggedUser, int>> scoresVector;
	std::map<LoggedUser, int> scores = this->m_database->getHighScores();
	std::map<LoggedUser, int>::iterator it = scores.begin();
	for (; it != scores.end(); it++)
	{
		scoresVector.push_back(std::make_pair(it->first, it->second));
	}
	return scoresVector;
}
HighScoreTable::~HighScoreTable()
{
	delete(this->m_database);
}
UserStats HighScoreTable::getUsesrStat(std::string name)
{
	return this->m_database->getUserStats(name);
}