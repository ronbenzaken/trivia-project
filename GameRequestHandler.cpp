#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(Room room, LoggedUser user) : m_user(user), m_room(room)
{
	gameId = m_gameManager.createGame(room);
}
bool GameRequestHandler::isRequestRelevant(Request req)
{
	if ((req.id == leaveGameR) || (req.id == getGameResR) || (req.id == getQuestionR) || (req.id == submitAnswerR))
	{
		return true;
	}
	return false;
}
RequestResult GameRequestHandler::handleRequest(Request req)
{
	RequestResult result;
	if (req.id == leaveGameR)
	{
		result = this->leaveGame(req);
	}
	else if (req.id == getGameResR)
	{
		result = this->getGameResult(req);
	}
	else if(req.id == getQuestionR)
	{
		result = this->getQuestion(req);
	}
	else
	{
		result = this->submitAnswer(req);
	}
	return result;
}
RequestResult GameRequestHandler::getQuestion(Request req)
{
	RequestResult ans;
	GetQuestionResponse res;
	res.status = goodResponse;
	Question ques = m_gameManager.getQuestionForUser(gameId, m_user);
	res.question = ques.getQues();
	res.answers.push_back(ques.getAns());
	res.answers.push_back(ques.getWAns1());
	res.answers.push_back(ques.getWAns2());
	res.answers.push_back(ques.getWAns3());
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	//ans.newHandler = (IRequestHandler*)m_handlerFactory.createGameRequestHandler(m_user, m_room);
	return ans;
}
RequestResult GameRequestHandler::submitAnswer(Request req)
{
	RequestResult ans;
	SubmitAnswerResponse res;
	SubmitAnswerRequest request = JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(req.buffer);
	res.status = goodResponse;
	res.correctAnswerId = 0;
	m_gameManager.submitAnswer(gameId, m_user, request.answerId);
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	//ans.newHandler = (IRequestHandler*)m_handlerFactory.createGameRequestHandler(m_user, m_room);
	return ans;
}
RequestResult GameRequestHandler::getGameResult(Request req)
{
	RequestResult ans;
	getGameResultResponse res;
	res.status = goodResponse;
	res.results = m_gameManager.getGameResult(gameId);
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	//ans.newHandler = (IRequestHandler*)m_handlerFactory.createGameRequestHandler(m_user, m_room);
	return ans;

}
RequestResult GameRequestHandler::leaveGame(Request req)
{
	RequestResult ans;
	LeaveGameResponse res;
	res.status = goodResponse;
	m_room.removeUser(m_user);
	m_gameManager.removeUser(gameId, m_user);
	ans.response = JsonResponsePacketSerializer::serializeResponse(res);
	ans.newHandler = (IRequestHandler*)m_handlerFactory.createMenuRequestHandler(m_user);
	return ans;
}