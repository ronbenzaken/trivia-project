#pragma once
#include <list>
#include <string>
#include <io.h>
#include <process.h>
#include <vector>
#include <direct.h>
#include "IDataAccess.h"
#include <iostream>
#include <utility>
#include <functional>
#include <algorithm>
#include "sqlite3.h"


using std::string;
using std::cout;
using std::endl;
using std::list;

class DataBaseAccess : public IDataAccess
{
private:
	sqlite3 * db;
public:
	DataBaseAccess();
	list<Question> getQuestions(int) override;
	std::vector<string> splitBuffer(std::string str, char seperate);
	void insertQuestions();
	bool doesUserExist(const string) override;
	void insertNewUser(const NewUser newUser) override;
	bool open() override;
	void increaseGame(const string name) override;
	void increaseRAns(const string name, int) override;
	void increaseWGame(const string name) override;
	void increaseWAns(const string name, int) override;
	std::map<LoggedUser, int> getHighScores() override;
	UserStats getUserStats(string name) override;
	bool login(string name, string pass) override;


};

