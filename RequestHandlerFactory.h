#pragma once
#include <iostream>
#include "LoginManager.h"
#include "RoomManager.h"
#include "HighScoreTable.h"
#include "GameManager.h"
/*//#include "LoginRequestHandler.h"*/

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
private:
	HighScoreTable m_highScoreTable;
	LoginManager m_loginManager;
	RoomManager m_roomManager;
	GameManager m_gameManager;
public:
	MenuRequestHandler* createMenuRequestHandler(LoggedUser);
	LoginRequestHandler* createLoginRequestHandler();
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser , Room);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser loggedUser, Room room);
	GameRequestHandler* createGameRequestHandler(LoggedUser user, Room room);
};
