#pragma once
#include "Response.h"
#include "UserStats.h"
typedef struct GetPlayerScoresResponse : public Response
{
	UserStats stats;
}GetPlayerScoresResponse;