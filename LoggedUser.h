#pragma once
#include <iostream>
#include <string>

class LoggedUser
{
private:
	std::string name;
public:
	LoggedUser(const std::string name);
	std::string getName() const;
	bool operator<(const LoggedUser& a) const;
	bool operator==(const LoggedUser& a) const;

};
