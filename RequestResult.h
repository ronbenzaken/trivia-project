#pragma once

#include "IRequestHandler.h"
class IRequestHandler;
struct RequestResult
{
public:
	Buffer response;
	IRequestHandler* newHandler;

} typedef RequestResult;
