#include "JsonRequestPacketDeserializer.h"
#define ROOM_NAME 0
#define MAX_USERS 1
#define QUES_COUNT 2
#define Q_TIMEOUT 3
using std::vector;
using std::string;
using std::stoi;
vector<string> splitBuffer(Buffer str, char seperate)
{
	int i = 0, size = str.size();
	vector<string> separatedStr;
	string tempStr;
	for (i = 0; i < size; i++)
	{
		if (str[i] == seperate)
		{
			separatedStr.push_back(tempStr);
			tempStr.clear();
		}
		else
			tempStr.push_back(str[i]);
	}
	separatedStr.push_back(tempStr);
	return separatedStr;
}

LoginRequest  JsonRequestPacketDeserializer::deserializerLoginRequest(Buffer req)
{
	//json j  = json::from_bson(req);
	//return j.get<LoginRequest>();
	LoginRequest result;
	vector<string> ans = splitBuffer(req, '#');
	result.userName = ans[0];
	result.password = ans[1];
	return result;
}
SignupRequest  JsonRequestPacketDeserializer::deserializerSignupRequest(Buffer req)
{

	//json j = json::from_bson(req);
	//return j.get<SignupRequest>();
	SignupRequest result;
	vector<string> ans = splitBuffer(req, '#');
	result.userName = ans[0];
	result.password = ans[1];
	result.email = ans[2];
	return result;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializerCreateRoomRequest(Buffer req)
{
	CreateRoomRequest result;
	vector<string> ans = splitBuffer(req, '#');
	result.roomName = ans[0];
	result.maxUsers = stoi(ans[1]);
	result.questionCount = stoi(ans[2]);
	result.answerTimeout = stoi(ans[3]);
	return result;
}
JoinRoomRequest JsonRequestPacketDeserializer::deserializerJoinRoomRequest(Buffer req)
{
	
	JoinRoomRequest result;
	result.roomId = stoi(req);
	return result;
}
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializerGetPlayersInRoomRequest(Buffer req)
{
	GetPlayersInRoomRequest result;
	result.roomId = stoi(req);
	return result;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(Buffer req)
{
	SubmitAnswerRequest result;
	result.answerId = stoi(req);
	return result;
}