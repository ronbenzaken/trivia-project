#include "Game.h"

Question Game::getQuestionForUser(LoggedUser user)
{
	Question a;
	a.setAns(m_players[user].currentQues.getAns());
	a.setQues(m_players[user].currentQues.getQues());
	a.setWAns1(m_players[user].currentQues.getWAns1());
	a.setWAns2(m_players[user].currentQues.getWAns2());
	a.setWAns3(m_players[user].currentQues.getWAns3());
	return a;
}
void Game::removePlayer(LoggedUser user)
{
	m_players.erase(user);
}
bool Game::submitAnswer(LoggedUser user, int ansId)
{
	int i = 0;
	string qu;
	Question temp = m_players[user].currentQues;
	qu = temp.getQues();
	for (i = 0; i + 1 < m_question.size(); i++)
	{
		if (qu == m_question[i].getQues())
		{
			m_players[user].currentQues.setAns(m_question[i + 1].getAns());
			m_players[user].currentQues.setQues(m_question[i + 1].getQues());
			m_players[user].currentQues.setWAns1(m_question[i + 1].getWAns1());
			m_players[user].currentQues.setWAns2(m_question[i + 1].getWAns2());
			m_players[user].currentQues.setWAns3(m_question[i + 1].getWAns3());
		}
	}
	if (ansId == 0)
	{
		m_players[user].rightAnswerCount++;
		return true;
	}
	else
	{
		m_players[user].wrongAnswerCount++;
		return false;
	}
}
int Game::getPlayerNum()
{
	return m_players.size();
}
std::vector<PlayerResult> Game::getGameResult()
{
	std::vector<PlayerResult> GameResult;
	PlayerResult temp;
	for (auto it = m_players.begin(); it != m_players.end(); it++)
	{
		temp.userName = (*it).first.getName();
		temp.rightAnswerCount = (*it).second.rightAnswerCount;
		temp.wrongAnswerCount = (*it).second.wrongAnswerCount;
		GameResult.push_back(temp);
	}
	return GameResult;
}
int Game::getGameId()
{
	return gameId;
}
Game::Game(std::vector<Question> q, int id)
{
	this->m_question = q;
	this->gameId = id;
}
Game::Game()
{

}
bool Game::operator==(Game other)
{
	if (this->gameId == other.gameId)
	{
		return true;
	}
	return false;
}
void Game::addPlayer(LoggedUser user)
{
	GameData temp(m_question[0]);
	temp.rightAnswerCount = 0;
	temp.wrongAnswerCount = 0;
	this->m_players[user] = temp;
	this->m_players[user].currentQues.setQues(m_question[0].getQues());
	this->m_players[user].currentQues.setWAns3(m_question[0].getWAns3());
	this->m_players[user].currentQues.setWAns2(m_question[0].getWAns2());
	this->m_players[user].currentQues.setWAns1(m_question[0].getWAns1());
	this->m_players[user].currentQues.setAns(m_question[0].getAns());



}
