#include "ClientsMapSocket.h"	

//int RoomManager::id = 0;
map<LoggedUser, SOCKET> ClientsMapSocket::clients;

void ClientsMapSocket::sendData(string message, SOCKET sc)
{
	const char* data = message.c_str();
	if (send(sc, message.c_str(), message.size(), 0) == INVALID_SOCKET)
	{
		throw ("Error while sending message to client");
	}
}