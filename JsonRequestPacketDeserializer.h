#pragma once
#include "LoginRequest.h"
#include "SignupRequest.h"
#include "JoinRoomRequest.h"
#include "CreateRoomRequest.h"
#include "SubmitAnswerRequest.h"
#include "GetPlayersInRoomRequest.h"
#include "Request.h"
#include <string>

class  JsonRequestPacketDeserializer
{
public:
	static LoginRequest  deserializerLoginRequest(Buffer req);
	static SignupRequest  deserializerSignupRequest(Buffer req);
	static CreateRoomRequest deserializerCreateRoomRequest(Buffer req);
	static JoinRoomRequest deserializerJoinRoomRequest(Buffer req);
	static GetPlayersInRoomRequest deserializerGetPlayersInRoomRequest(Buffer req);
	static SubmitAnswerRequest deserializerSubmitAnswerRequest(Buffer req);
};
