#pragma once
#include <iostream>
typedef struct RoomData
{
	unsigned int id;
	std::string name;
	std::string maxPlayers;
	unsigned int timePerQuestion;
	unsigned int questionCount;
	int isActive;
	RoomData& operator =(const RoomData& data)
	{
		this->questionCount = data.questionCount;
		this->id = data.id;
		this->isActive = data.isActive;
		this->maxPlayers = data.maxPlayers;
		this->name = data.name;
		this->timePerQuestion = data.timePerQuestion;
		return *this;
	}
}RoomData;