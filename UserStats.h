#pragma once
class UserStats
{
private:
	int gamesWon;
	int RAns;
	int WAns;
	int games;
public:
	UserStats(const int game, const int gamesW, const int rAns, const int wAns);
	int getGamesWon()const;
	int getGames()const;
	int getRAns()const;
	int getWAns() const;
	UserStats(const UserStats &other);
	UserStats();
};