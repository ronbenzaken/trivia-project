#pragma once
#include <vector>
#include "LoggedUser.h"
#include "RoomData.h"
#include <algorithm>

class Room {

private:

	std::vector<LoggedUser>  m_users;
	RoomData m_metadata;
public:
	RoomData getRoomData();
	Room(RoomData data);
	void changeRoomState();
	Room();
	Room(const Room &room);
	int addUser(LoggedUser user);
	int removeUser(LoggedUser user);
	std::vector<LoggedUser> getAllUsers();
};
