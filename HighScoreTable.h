#pragma once
#include <vector>
#include "IDataAccess.h"
class  HighScoreTable
{

private:
	IDataAccess* m_database;

public:
	~HighScoreTable();
	HighScoreTable();
	std::vector<std::pair<LoggedUser, int>> getHighScores();
	UserStats getUsesrStat(std::string name);

};
