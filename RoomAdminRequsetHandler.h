#pragma once

#include "IRequestHandler.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"
#include "Room.h"

class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(Room room, LoggedUser loggedUser);
	virtual bool isRequestRelevant(Request req);
	virtual RequestResult handleRequest(Request req);
private:
	Room m_room;
	LoggedUser m_user;
	RoomManager m_roomManager;
	RequestHandlerFactory m_handlerFacroty;
	RequestResult closeRoom(Request req);
	RequestResult startGame(Request req);
	RequestResult getRoomState(Request req);
};