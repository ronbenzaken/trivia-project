#pragma once

#include "Response.h"
#include "Room.h"
typedef struct GetRoomsResponse : public Response
{
	std::vector<RoomData> rooms;
} GetRoomResponse;