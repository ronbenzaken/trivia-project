#include "LoginManager.h"

void LoginManager::signUp(std::string userName, std::string password, std::string email)
{
	if (m_database.doesUserExist(userName))
	{
		throw(std::string("User Already Exict Choose Diffrent User Name"));
	}
	else
	{
		NewUser user(userName, password, email);
		m_database.insertNewUser(user);
	}
}

void LoginManager::logIn(std::string userName, std::string password)
{
	
	if (m_database.login(userName, password))
	{
		LoggedUser loggedUser(userName);
		m_loggedUsers.push_back(userName);
	}
	else
	{
		throw(std::string("User Name Or Password Incorrect"));
	}
}

void LoginManager::logOut()
{
	m_loggedUsers.clear();
}