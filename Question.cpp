#include "Question.h"
#include <string>
Question::Question(string ques, string ans, string WAns1, string WAns2, string WAns3)
{
	this->ques = ques;
	this->ans = ans;
	this->WAns1 = WAns1;
	this->WAns2 = WAns2;
	this->WAns3 = WAns3;
}
string Question::getQues() const { return ques; }
string Question::getAns() const { return ans; }
string Question::getWAns1() const { return WAns1; }
string Question::getWAns2() const { return WAns2; }
string Question::getWAns3() const { return WAns3; }

void Question::setQues(string a) { this->ques = a; }
void Question::setAns(string a) { this->ans = a; }
void Question::setWAns1(string a) { this->WAns1 = a; }
void Question::setWAns2(string a) { this->WAns2 = a; }
void Question::setWAns3(string a){ this->WAns3 = a ; }

Question Question::operator=(Question ot)
{
	Question ques(ot.getQues(), ot.getAns(), ot.getWAns1(), ot.getWAns2(), ot.getWAns3());
	return ques;
}
