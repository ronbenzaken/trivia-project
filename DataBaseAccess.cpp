#include "DataBaseAccess.h"
#include <fstream>

int callbackQuestion(void * data, int argc, char** argv, char** azColName)
{
	int i;
	string ques, ans, Wans1, Wans2, Wans3;
	for (i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "QUESTION")
		{
			ques = argv[i];
		}
		else if(string(azColName[i]) == "ANS")
		{
			ans = argv[i];
		}
		else if (string(azColName[i]) == "WRONG_ANS1")
		{
			Wans1 = argv[i];
		}
		else if (string(azColName[i]) == "WRONG_ANS2")
		{
			Wans2= argv[i];
		}
		else if (string(azColName[i]) == "WRONG_ANS3")
		{
			Wans3 = argv[i];
		}
	}
	(*((list<Question>*)(data))).push_back(Question(ques, ans, Wans1, Wans2, Wans3));
	return 0;
}

int callbackHighScores(void * data, int argc, char** argv, char** azColName)
{
	int rAns, i;
	string name;
	
	for (i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "USER_NAME")
		{
			name = argv[i];
		}
		else if (string(azColName[i]) == "R_ANS")
		{
			rAns = std::atoi(argv[i]);
		}
	}
	(*((std::map<LoggedUser, int>*)(data))).insert(std::make_pair(LoggedUser(name), rAns));
	return 0;
}
int callbackStat(void * data, int argc, char** argv, char** azColName)
{
	int game, gamesW, rAns, wAns, i;
	for (i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "GAMES")
		{
			game = std::atoi(argv[i]);
		}
		else if (string(azColName[i]) == "GAMES_W")
		{
			gamesW = std::atoi(argv[i]);
		}
		else if (string(azColName[i]) == "R_ANS")
		{
			rAns = std::atoi(argv[i]);
		}
		else if (string(azColName[i]) == "W_ANS")
		{
			wAns = std::atoi(argv[i]);
		}
	}
	(*((UserStats*)(data))) = UserStats(game, gamesW, rAns, wAns);
	return 0;
}
int callbackNum(void * data, int argc, char** argv, char** azColName)
{
	
	*(int*)data = atoi(argv[0]);
	return 0;
}
DataBaseAccess::DataBaseAccess()
{
	this->open();
}
list<Question> DataBaseAccess::getQuestions(int QNum) 
{
	list<Question> questions;
	string sqlStatement = "SELECT * FROM QUES LIMIT " + std::to_string(QNum) + ";";
	char ** errM = nullptr;
	int res;
	int ans;
	res = sqlite3_exec(db, sqlStatement.c_str(), callbackQuestion, (void*)&questions, errM);
	return questions;
}
std::vector<string> DataBaseAccess::splitBuffer(std::string str, char seperate)
{
	int i = 0, size = str.size();
	std::vector<string> separatedStr;
	string tempStr;
	for (i = 0; i < size; i++)
	{
		if (str[i] == seperate)
		{
			separatedStr.push_back(tempStr);
			tempStr.clear();
		}
		else
			tempStr.push_back(str[i]);
	}
	separatedStr.push_back(tempStr);
	return separatedStr;
}
void DataBaseAccess::insertQuestions() 
{
	std::ifstream file;
	file.open("ques.txt");
	std::string line;
	std::vector<string> data;
	string sqlStatement;
	char** errM = NULL;
	int res;
	////////////
	while (std::getline(file, line))
	{
		data = this->splitBuffer(line, '#');
		sqlStatement = "INSERT INTO QUES VALUES(NULL, \"" + data[0] + "\", \"" + data[1] + "\", \"" + data[2] + "\", \"" + data[3] + "\", \"" + data[4] + "\");";
		std::cout << sqlStatement;
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);
	}
}
bool DataBaseAccess::open()
{
	string dbFileName = "TriviaProjectDB.sqlite";
	string sqlStatement;
	char ** errM = nullptr;
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK) {
		db = nullptr;
		cout << "Failed to open DB" << endl;
		return false;
	}
	if (doesFileExist == -1) 
	{
		sqlStatement = "CREATE TABLE USERS(ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT , NAME TEXT NOT NULL , EMAIL TEXT NOT NULL , PASS TEXT NOT NULL);";
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);
		if (res != SQLITE_OK)
		{
			return false;
		}
		sqlStatement = "CREATE TABLE QUES(ID INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, QUESTION TEXT NOT NULL , ANS TEXT NOT NULL , WRONG_ANS1 TEXT NOT NULL , WRONG_ANS2 TEXT NOT NULL , WRONG_ANS3 TEXT NOT NULL);";
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);
		if (res != SQLITE_OK)
		{
			return false;
		}
		sqlStatement = "CREATE TABLE STATS(ID INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT , GAMES INTEGER NOT NULL,  W_ANS NOT NULL, R_ANS INTEGER NOT NULL, GAMES_W INTEGER NOT NULL, USER_NAME TEXT NOT NULL , FOREIGN KEY (USER_NAME)  REFERENCES USERS(NAME));";
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);
		if (res != SQLITE_OK)
		{
			return false;
		}
		insertQuestions();
	}
	return true;
}
bool DataBaseAccess::doesUserExist(const string name)
{
	string sqlStatement = "SELECT COUNT(ID) FROM USERS WHERE NAME = \"" + name + "\";";
	char ** errM = nullptr;
	int res;
	int ans;
	res = sqlite3_exec(db, sqlStatement.c_str(), callbackNum, (void*)&ans, errM);
	if (ans > 0)
	{
		return true;
	}
	return false;
}
void DataBaseAccess::insertNewUser(const NewUser newUser)
{
	int res;
	char ** errM = nullptr;
	string sqlStatement;
	sqlStatement = "INSERT INTO USERS VALUES(NULL, \"" + newUser.getName() + "\", \"" + newUser.getEmail() + "\", \"" + newUser.getPass() + "\");";
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);
	sqlStatement = "INSERT INTO STATS VALUES(NULL, 0, 0, 0, 0, \"" + newUser.getName() + "\");";
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);
}
void DataBaseAccess::increaseGame(const string name)
{
	int res, currentGames;
	char ** errM = nullptr;
	string sqlStatement;
	sqlStatement = "SELECT GAMES FROM STATS WHERE USER_NAME = \"" + name + "\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), callbackNum, (void*)&currentGames, errM);
	currentGames++;
	sqlStatement = "UPDATE STATS SET GAMES = " + std::to_string(currentGames) + " WHERE USER_NAME = \"" + name + "\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);

}
void DataBaseAccess::increaseRAns(const string name, int rAnsCount)
{
	int res, currentRAns;
	char ** errM = nullptr;
	string sqlStatement;
	sqlStatement = "SELECT R_ANS FROM STATS WHERE USER_NAME = \"" + name + "\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), callbackNum, (void*)&currentRAns, errM);
	currentRAns+= rAnsCount;
	sqlStatement = "UPDATE STATS SET R_ANS = " + std::to_string(currentRAns) + " WHERE USER_NAME = \"" + name + "\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);

}
void DataBaseAccess::increaseWGame(const string name)
{
	int res, currentGamesWon;
	char ** errM = nullptr;
	string sqlStatement;
	sqlStatement = "SELECT GAMES_W FROM STATS WHERE USER_NAME = \"" + name + "\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), callbackNum, (void*)&currentGamesWon, errM);
	currentGamesWon++;
	sqlStatement = "UPDATE STATS SET GAMES_W = " + std::to_string(currentGamesWon) + " WHERE USER_NAME = \"" + name + "\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);


}
void DataBaseAccess::increaseWAns(const string name, int wAnsCount)
{
	int res, currentWAns;
	char ** errM = nullptr;
	string sqlStatement;
	sqlStatement = "SELECT W_ANS FROM STATS WHERE USER_NAME = \"" + name + "\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), callbackNum, (void*)&currentWAns, errM);
	currentWAns += wAnsCount;
	sqlStatement = "UPDATE STATS SET W_ANS = " + std::to_string(currentWAns) + " WHERE USER_NAME = \"" + name + "\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errM);

}
std::map<LoggedUser, int> DataBaseAccess::getHighScores()
{
	int res, currentWAns;
	std::map < LoggedUser, int> highScores;
	char ** errM = nullptr;
	string sqlStatement;
	sqlStatement = "SELECT * FROM STATS ORDER BY R_ANS DESC LIMIT 4;";
	res = sqlite3_exec(db, sqlStatement.c_str(), callbackHighScores, (void*)&highScores, errM);
	return highScores;
}
UserStats DataBaseAccess::getUserStats(string name)
{
	int res, currentWAns;
	char ** errM = nullptr;
	string sqlStatement;
	UserStats userStats;
	sqlStatement = "SELECT * FROM STATS WHERE USER_NAME = \"" + name + "\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), callbackStat, (void*)&userStats, errM);
	return userStats;
}
bool DataBaseAccess::login(string name, string pass)
{
	int res, currentWAns, ans;
	char ** errM = nullptr;
	string sqlStatement;
	UserStats userStats;
	sqlStatement = "SELECT COUNT(ID) FROM USERS WHERE NAME = \"" + name + "\" AND PASS = \"" + pass +"\";";
	res = sqlite3_exec(db, sqlStatement.c_str(), callbackNum, (void*)&ans, errM);
	if (ans > 0)
	{
		return true;
	}
	return false;
}
